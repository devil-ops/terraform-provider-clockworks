#!/bin/bash
set -x
set -e

PWD=$(pwd)
cd sandbox
terraform destroy
cd "$PWD"
