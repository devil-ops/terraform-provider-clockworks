terraform {
  required_providers {
    clockworks = {
      version = "0.0.1"
      source = "gitlab.oit.duke.edu/devil-ops/clockworks"
    }
  }
}

provider "clockworks" {
}

data "clockworks_location" "test" {
	display_name = "ATC Lab"
}

data "clockworks_os" "test" {
	os_id = "ssiRHEL7"
}
output "clockworks_os" {
	value = data.clockworks_os.test
}
/*
output "clockworks_location" {
  value = data.clockworks_location.test
}
*/

data "clockworks_vm" "test" {
	name = "drews-dev-sdk-01.oit.duke.edu"
}

/*
output "clockworks_vm" {
  value = data.clockworks_vm.test
}
*/

/*
resource "clockworks_vm" "test" {
	name                 = "drews-test-terraform-03.oit.duke.edu"
	cpu                  = 1
	ram                  = 2
	location             = "ATC Lab"
	//fund_code            = "000-9332"
	fund_code            = "000-3049"
	storage_type         = "silver"
	sysadmin_option_id   = 2
	container            = "Drew"
	os_id                = "ssi-centos-8"
	network              = "private"
	cloud_init_user_data = "#cloud-config"
	disk {
		size_gb = 50
	}
}
*/
resource "clockworks_vm" "self-test" {
	name                 = "drews-test-terraform-selfy-01.oit.duke.edu"
	cpu                  = 1
	ram                  = 2
	location             = "ATC Lab"
	//fund_code            = "000-9332"
	fund_code            = "000-3049"
	storage_type         = "silver"
	sysadmin_option_id   = 1
	container            = "Drew"
	os_id                = "ubuntu18"
	network              = "private"
	cloud_init_user_data = "#cloud-config"

	disk {
		order = 1
		size_gb = 51
	}
	disk {
		order = 2
		size_gb = 160
	}
}