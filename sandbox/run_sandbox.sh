#!/bin/bash
set -x
set -e

PWD=$(pwd)
./make.sh
rm -f sandbox/.terraform.lock.hcl
cd sandbox
##rm -rf .terraform .terraform.lock.hcl terraform.tfstate terraform.tfstate.backup
terraform init && terraform apply
cd "$PWD"
