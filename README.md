# Terraform Provider Clockworks

Use Terraform to provision and query things in Clockworks!

## Usage

Configure terraform to use this plugin with the following configuration

Unfortunately, we don't have this published to any official terraform
registries, so you'll need to download the plugin to your local filesytem. You
can do that by using something similar to:

```bash
$ export VERSION=0.2.2
$ export OS=darwin
$ export ARCH=amd64
$ mkdir -p ~/.terraform.d/plugins/gitlab.oit.duke.edu/devil-ops/clockworks/${VERSION}/${OS}_${ARCH}/
$ wget https://gitlab.oit.duke.edu/devil-ops/terraform-provider-clockworks/-/releases/v${VERSION}/downloads/terraform-provider-clockworks_${VERSION}_${OS}_${ARCH}.zip -O /tmp/clockworks.zip
$ cd /tmp
$ unzip clockworks.zip
$ mv ./terraform-provider-clockworks_v${VERSION} ~/.terraform.d/plugins/gitlab.oit.duke.edu/devil-ops/clockworks/${VERSION}/${OS}_${ARCH}/terraform-provider-clockworks
```

Once we have this in an official terraform registry of some sort, we won't need
the steps above

```hcl
terraform {
  required_providers {
    clockworks = {
      source  = "gitlab.oit.duke.edu/devil-ops/clockworks"
    }
  }
}
```

Check out the examples in `sandbox/` for working examples.

Full documentation at [docs/](docs/).

## Caveats

For SSI managed hosts, we are setting `skip_finalize` option to `true` so that
we don't need to close a SNow ticket for each creation.

Disk changes aren't working quite right yet. Growing disks mostly works, but
attempting to shrink them will put you in a bad state, better to just rebuild if
the disks need to shrink. In a future version, the rebuild can be in place by
terraform, but it's not working yet.

## Old Version

If you are looking for the previous version, you may find here
[here](https://gitlab.oit.duke.edu/devil-ops/terraform-provider-clockworks-legacy)
