resource "clockworks_vm" "self-test" {
	name                 = "test-01.example.com"
	cpu                  = 1
	ram                  = 2
	location             = "ACME Lab"
	fund_code            = "000-1234"
	storage_type         = "silver"
	sysadmin_option_id   = 1
	container            = "Somewhere"
	os_id                = "ubuntu18"
	network              = "private"
	cloud_init_user_data = "#cloud-config"

	disk {
		order = 1
		size_gb = 50
	}
	disk {
		order = 2
		size_gb = 100
	}
}

resource "clockworks_vm" "batch-hosts" {
    name                 = "server-${format("%02d", count.index + 1)}.example.com"
    cpu                  = 1
    ram                  = 1
    location             = "ACME Lab"
    fund_code            = "000-1234"
    storage_type         = "bronze"
    sysadmin_option_id   = 2
    count                = 5
    os_id                = "abc-centos-8"
    network              = "private"
    cloud_init_user_data = "#cloud-config"

    disk {
        order = 1
        size_gb = 50
    }
}