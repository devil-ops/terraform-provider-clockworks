terraform {
  required_providers {
    clockworks = {
      source  = "gitlab.oit.duke.edu/devil-ops/clockworks"
    }
  }
}