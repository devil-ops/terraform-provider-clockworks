package clockworks

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	clockworksAPI "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func dataSourceLocation() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceLocationRead,
		Schema: map[string]*schema.Schema{
			"display_name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Display name",
			},
			"cluster_name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Cluster Name",
			},
			"vcenter_fqdn": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "VCenter FQDN",
			},
			"type": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Type",
			},
			"tags": {
				Type:        schema.TypeList,
				Computed:    true,
				Description: "Tags",
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
		},
	}
}

func dataSourceLocationRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*clockworksAPI.Client)
	var diags diag.Diagnostics

	dname := d.Get("display_name").(string)
	idMap, _, err := c.Location.DisplayNameMap(ctx)
	if err != nil {
		return diag.FromErr(err)
	}
	locationId := idMap[dname]

	if locationId == 0 {
		return diag.Errorf("Location %s not found", dname)
	}
	/*if err := d.Set("id", locationId); err != nil {
		return diag.FromErr(err)
	}*/

	location, _, err := c.Location.Get(ctx, locationId)
	if err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("display_name", location.DisplayName); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("cluster_name", location.ClusterName); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("tags", location.Tags); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("vcenter_fqdn", location.VCenterFQDN); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("type", location.Type); err != nil {
		return diag.FromErr(err)
	}
	// d.SetId(strconv.FormatInt(time.Now().Unix(), 10))
	sum := sha256.Sum256([]byte(dname))
	d.SetId(fmt.Sprintf("%x", sum))

	return diags
}
