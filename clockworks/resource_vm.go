package clockworks

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/apex/log"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/customdiff"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/validation"
	clockworksAPI "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func resourceVM() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceVMCreate,
		ReadContext:   resourceVMRead,
		UpdateContext: resourceVMUpdate,
		DeleteContext: resourceVMDelete,
		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(2 * time.Hour),
			Update: schema.DefaultTimeout(1 * time.Hour),
			Delete: schema.DefaultTimeout(1 * time.Hour),
		},
		Importer: &schema.ResourceImporter{
			StateContext: schema.ImportStatePassthroughContext,
		},
		CustomizeDiff: customdiff.All(
			// customdiff.ForceNewIfChange("disk", func(ctx context.Context, old, new, meta interface{}) bool {
			// customdiff.ForceNewIf("disk", func(ctx context.Context, d *schema.ResourceDiff, meta interface{}) bool {
			customdiff.ForceNewIfChange("disk", func(ctx context.Context, old, new, meta interface{}) bool {
				// This should force the entire resource to need recreation, but that's not working...dunno why...
				newv := new.([]interface{})
				log.Infof("META: %+v", meta)
				for i, thing := range newv {
					newd := thing.(map[string]interface{})
					// Do we have old disks?
					if len(old.([]interface{})) > 0 {
						oldd := old.([]interface{})[i].(map[string]interface{})
						if newd["size_gb"].(int) < oldd["size_gb"].(int) {
							log.Warn("Disk size is decreasing, FORCE THAT CHAAAAGE")
							return true
						}
					}
				}
				return false
			}),
		),
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Name of the VM (Usually the FQDN)",
				ForceNew:    true,
			},
			"fund_code": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Fund Code",
			},
			"cloud_init_user_data": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Cloud Init User Data",
				ForceNew:    true,
				DiffSuppressFunc: func(k, old, new string, d *schema.ResourceData) bool {
					// IF we never knew what the request was, don't change it
					if old == "" {
						return true
					}
					return false
				},
			},
			"cpu": {
				Type:        schema.TypeInt,
				Description: "CPU Cores",
				Optional:    true,
				Default:     1,
			},
			"ram": {
				Type:        schema.TypeInt,
				Description: "RAM in GB",
				Optional:    true,
				Default:     1,
			},
			"location": {
				Type:        schema.TypeString,
				Description: "Location of the VM",
				Required:    true,
				ForceNew:    true,
			},
			"storage_type": {
				Type:        schema.TypeString,
				Description: "Type of Storage",
				Required:    true,
			},
			"container": {
				Type:        schema.TypeString,
				Description: "Container for Self-Admin VMs",
				Optional:    true,
				ForceNew:    true,
				DiffSuppressFunc: func(k, old, new string, d *schema.ResourceData) bool {
					if strings.HasSuffix(old, fmt.Sprintf("/%v", new)) {
						return false
					}
					return true
				},
			},
			"sysadmin_option_id": {
				Type:        schema.TypeInt,
				Description: "Sysadmin Option Id",
				Required:    true,
				ForceNew:    true,
			},
			"guest_fqdn": {
				Type:        schema.TypeString,
				Description: "Guest FQDN",
				Computed:    true,
			},
			"patch_window_id": {
				Type:        schema.TypeInt,
				Description: "Patch Window Id",
				Optional:    true,
				Default:     -1,
				ForceNew:    true,
				DiffSuppressFunc: func(k, old, new string, d *schema.ResourceData) bool {
					if new == "-1" {
						return true
					}
					return false
				},
			},
			"os_id": {
				Type:        schema.TypeString,
				Description: "Operating System Id",
				Required:    true,
				ForceNew:    true,
			},
			"network": {
				Type:         schema.TypeString,
				Description:  "Network (public, private or custom)",
				Required:     true,
				ForceNew:     true,
				ValidateFunc: validation.StringInSlice([]string{"public", "private", "custom"}, false),
				DiffSuppressFunc: func(k, old, new string, d *schema.ResourceData) bool {
					// IF we never knew what the request was, don't change it
					if old == "" {
						return true
					}
					return false
				},
			},
			"web_url": {
				Type:        schema.TypeString,
				Description: "Web URL",
				Computed:    true,
				ForceNew:    true,
			},
			"disk": {
				// Type: schema.TypeSet,
				Type:        schema.TypeList,
				Required:    true,
				Description: "List of disks",
				MinItems:    1,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"order": {
							Type:        schema.TypeInt,
							Description: "Order in which to show up",
							Required:    true,
						},
						"disk_id": {
							Type:     schema.TypeInt,
							Computed: true,
						},
						"size_gb": {
							Type:     schema.TypeInt,
							Required: true,
						},
					},
				},
			},
		},
		// CustomizeDiff: customdiff.ForceNewIfChange("disk", func(ctx context.Context, old, new, meta interface{}) bool {
	}
}

func resourceVMRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*clockworksAPI.Client)

	// Import from ID
	cid, err := strconv.Atoi(d.Id())
	if err != nil {
		return diag.FromErr(err)
	}
	vm, _, err := c.VM.Get(ctx, cid)
	if err != nil {
		return diag.FromErr(err)
	}
	memI := int(vm.MemGB)

	// Figure out location id
	location, _, err := c.Location.Get(ctx, vm.LocationId)
	if err != nil {
		return diag.Errorf("Could not get location")
	}

	shortcuts := map[string]interface{}{
		"name":       vm.Name,
		"cpu":        vm.CPU,
		"guest_fqdn": vm.GuestFqdn,
		"fund_code":  vm.FundCode,
		"ram":        memI,
		//"patch_window_id": vm.PatchWindowId,
		"os_id":              vm.OSId,
		"web_url":            vm.WebURL,
		"location":           location.DisplayName,
		"storage_type":       vm.StorageType,
		"sysadmin_option_id": vm.SysadminOptionId,
	}
	for k, v := range shortcuts {
		log.WithFields(log.Fields{
			"key":   k,
			"value": v,
		}).Debug("Shortcut settings stuff")
		if err := d.Set(k, v); err != nil {
			return diag.FromErr(err)
		}
	}

	// Handle disk mapping
	rDisks, err := vmDisksToDiskResource(vm.Disks)
	if err != nil {
		return diag.FromErr(err)
	}
	d.Set("disk", rDisks)

	return diags
}

func resourceVMDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*clockworksAPI.Client)
	clockworksId, err := strconv.Atoi(d.Id())
	if err != nil {
		return diag.FromErr(err)
	}
	req, _, err := c.VM.Delete(ctx, clockworksId, nil)
	if err != nil {
		return diag.FromErr(err)
	}
	err = c.VMRequest.WaitForFinalStatus(ctx, req.VMRequestId)
	if err != nil {
		return diag.FromErr(err)
	}
	d.SetId("")

	return diags
}

func resourceVMCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*clockworksAPI.Client)
	name := d.Get("name").(string)
	_, _, err := c.VM.GetWithFQDN(ctx, name)
	if err == nil {
		return diag.Errorf("VM with name %s already exists", name)
	}

	// Figure out location id
	locationM, _, err := c.Location.DisplayNameMap(ctx)
	if err != nil {
		return diag.Errorf("Could not get location display name map")
	}
	locationId := locationM[d.Get("location").(string)]

	// orderedTfDisks, err := diskResourceToTFDisk(d.Get("disk").([]interface{}).(List).List())
	orderedTfDisks, err := diskResourceToTFDisk(d.Get("disk").([]interface{}))
	if err != nil {
		return diag.FromErr(err)
	}
	disks, err := getVMDisksWithTFDisks(orderedTfDisks)
	if err != nil {
		return diag.FromErr(err)
	}

	opts := clockworksAPI.VMCreateOpts{
		Hostname:          name,
		Disks:             disks,
		LocationId:        locationId,
		CPU:               d.Get("cpu").(int),
		RAM:               d.Get("ram").(int),
		FundCode:          d.Get("fund_code").(string),
		StorageType:       d.Get("storage_type").(string),
		SysadminOptionId:  d.Get("sysadmin_option_id").(int),
		ContainerName:     d.Get("container").(string),
		OSId:              d.Get("os_id").(string),
		NetworkType:       d.Get("network").(string),
		CloudInitUserData: d.Get("cloud_init_user_data").(string),
		SkipFinalize:      true,
		PatchWindowId:     d.Get("patch_window_id").(int),
	}
	req, _, err := c.VM.Create(ctx, &opts)
	if err != nil {
		return diag.FromErr(err)
	}
	err = c.VMRequest.WaitForFinalStatus(ctx, req.VMRequestId)
	if err != nil {
		return diag.FromErr(err)
	}
	log.Warn("VM created, waiting a hot second before checking back")
	time.Sleep(time.Second * 3)

	// Refresh the final request to be sure we have the VM ID
	finalReq, _, err := c.VMRequest.Get(ctx, req.VMRequestId)
	if err != nil {
		return diag.FromErr(err)
	}

	// sum := sha256.Sum256([]byte(name))
	d.SetId(strconv.Itoa(finalReq.VmId))

	resourceVMRead(ctx, d, m)
	return diags
}

func resourceVMUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*clockworksAPI.Client)
	clockworksId, err := strconv.Atoi(d.Id())
	if err != nil {
		return diag.FromErr(err)
	}
	vm, _, err := c.VM.Get(ctx, clockworksId)
	if err != nil {
		return diag.Errorf("Could not find VM with clockworks id %d", clockworksId)
	}

	opt := clockworksAPI.VMEditOpts{
		// Always allow poweroff, we are automate heavy!
		PoweroffAllowed: true,
	}
	if d.HasChange("cpu") {
		opt.CPU = d.Get("cpu").(int)
	}
	if d.HasChange("ram") {
		opt.RAM = d.Get("ram").(int)
	}
	if d.HasChange("fund_code") {
		opt.FundCode = d.Get("fund_code").(string)
	}
	if d.HasChange("storage_type") {
		opt.StorageType = d.Get("storage_type").(string)
	}

	if d.HasChange("disk") {
		// vmds, err := getVMDisksWithDiskResource(d.Get("disk").(interface{}))
		vmds, err := getVMDisksWithDiskResource(d.Get("disk").([]interface{}))
		if err != nil {
			return diag.FromErr(err)
		}
		log.Warnf("DIIIISKS: %+v", vmds)
		opt.Disks = vmds
	}
	log.Infof("VM update opts: %+v", opt)
	req, _, err := c.VM.Edit(ctx, vm.Id, &opt)
	if err != nil {
		return diag.FromErr(err)
	}

	err = c.VMRequest.WaitForFinalStatus(ctx, req.VMRequestId)
	if err != nil {
		return diag.FromErr(err)
	}
	log.Warn("VM edited, waiting a host second before checking back")
	time.Sleep(time.Second * 10)

	// d.Set("last_updated", time.Now().Format(time.RFC850))

	resourceVMRead(ctx, d, m)
	return diags
}
