package clockworks

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func TestDiskOrdering(t *testing.T) {
	ts := []struct {
		disks         []TFDisk
		expectedSizes []int
		wantErr       bool
	}{
		{
			disks:         []TFDisk{{Order: 1, SizeGB: 100}},
			expectedSizes: []int{100},
		},
		{
			disks:         []TFDisk{{Order: 1, SizeGB: 50}, {Order: 2, SizeGB: 100}},
			expectedSizes: []int{50, 100},
		},
		{
			disks:         []TFDisk{{Order: 2, SizeGB: 50}, {Order: 1, SizeGB: 100}},
			expectedSizes: []int{100, 50},
		},
	}
	for _, tt := range ts {
		got, err := diskOrdering(tt.disks)
		var gotS []int
		for _, d := range got {
			gotS = append(gotS, d.SizeGB)
		}
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
			require.Equal(t, tt.expectedSizes, gotS)
		}
	}
}

func TestTFDiskValidation(t *testing.T) {
	ts := []struct {
		disks   []TFDisk
		wantErr bool
	}{
		{disks: []TFDisk{{Order: 1, SizeGB: 100}}},
		{disks: []TFDisk{{Order: 2, SizeGB: 100}}},
		{
			disks: []TFDisk{{Order: 1, SizeGB: 50}, {Order: 2, SizeGB: 100}},
		},
	}
	for _, tt := range ts {
		_, err := diskOrdering(tt.disks)
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
	}
}

func TestValidateTFDisks(t *testing.T) {
	ts := []struct {
		disks   []TFDisk
		wantErr bool
		msg     string
	}{
		{[]TFDisk{{Order: 0}}, true, "disk order cannot be 0"},
		{[]TFDisk{{Order: 1}}, false, "Single disk"},
		{[]TFDisk{{Order: 1}, {Order: 1}}, true, "disk order must be unique"},
		{[]TFDisk{{Order: 2}, {Order: 3}}, true, "disk order must start at 1"},
		{[]TFDisk{{Order: 1}, {Order: 3}}, true, "skipping order numbers not allowed"},
	}
	for _, tt := range ts {
		err := validateTFDisks(tt.disks)
		if tt.wantErr {
			require.Error(t, err, tt.msg)
		} else {
			require.NoError(t, err, tt.msg)
		}
	}
}

func TestDiskResourceToTFDisk(t *testing.T) {
	ts := []struct {
		disks   []interface{}
		want    []TFDisk
		wantErr bool
		msg     string
	}{
		{
			disks:   []interface{}{map[string]interface{}{"order": 1, "size_gb": 100}},
			want:    []TFDisk{{Order: 1, SizeGB: 100}},
			wantErr: false,
			msg:     "Single disk",
		},
		{
			disks:   []interface{}{map[string]interface{}{"order": 2, "size_gb": 100}, map[string]interface{}{"order": 1, "size_gb": 150}},
			want:    []TFDisk{{Order: 1, SizeGB: 150}, {Order: 2, SizeGB: 100}},
			wantErr: false,
			msg:     "Multiple disks",
		},
	}
	for _, tt := range ts {

		got, err := diskResourceToTFDisk(tt.disks)
		if tt.wantErr {
			require.Error(t, err, tt.msg)
		} else {
			require.Equal(t, tt.want, got, tt.msg)
			require.NoError(t, err, tt.msg)
		}
	}
}

func TestVMDiskToDiskResource(t *testing.T) {
	ts := []struct {
		disks   []clockworks.VMDisk
		want    []map[string]interface{}
		wantErr bool
		msg     string
	}{
		{
			disks: []clockworks.VMDisk{{SizeGB: 100}},
			// want:    []interface{}{map[string]interface{}{"order": 1, "size_gb": 100}},
			want:    []map[string]interface{}{{"disk_id": 0, "order": 1, "size_gb": 100}},
			wantErr: false,
			msg:     "Single disk",
		},
		{
			disks: []clockworks.VMDisk{{SizeGB: 150}, {SizeGB: 100}},
			// want:    []interface{}{map[string]interface{}{"order": 2, "size_gb": 100}, map[string]interface{}{"order": 1, "size_gb": 150}},
			want: []map[string]interface{}{
				{"disk_id": 0, "order": 1, "size_gb": 150},
				{"disk_id": 0, "order": 2, "size_gb": 100},
			},
			wantErr: false,
			msg:     "Multiple disks",
		},
	}
	for _, tt := range ts {

		got, err := vmDisksToDiskResource(tt.disks)
		if tt.wantErr {
			require.Error(t, err, tt.msg)
		} else {
			require.Equal(t, tt.want, got, tt.msg)
			require.NoError(t, err, tt.msg)
		}
	}
}

func TestGetVMDisksWithTFDisks(t *testing.T) {
	ts := []struct {
		disks   []TFDisk
		want    []clockworks.VMDisk
		wantErr bool
		msg     string
	}{
		{
			disks:   []TFDisk{{Order: 1, SizeGB: 100}},
			want:    []clockworks.VMDisk{{SizeGB: 100}},
			wantErr: false,
			msg:     "Single disk",
		},
		{
			disks:   []TFDisk{{Order: 1, SizeGB: 150}, {Order: 2, SizeGB: 100}},
			want:    []clockworks.VMDisk{{SizeGB: 150}, {SizeGB: 100}},
			wantErr: false,
			msg:     "Multiple disks",
		},
	}
	for _, tt := range ts {

		got, err := getVMDisksWithTFDisks(tt.disks)
		if tt.wantErr {
			require.Error(t, err, tt.msg)
		} else {
			require.Equal(t, tt.want, got, tt.msg)
			require.NoError(t, err, tt.msg)
		}
	}
}

func TestGetVMDisksWithDiskResources(t *testing.T) {
	ts := []struct {
		disks   []interface{}
		want    []clockworks.VMDisk
		wantErr bool
		msg     string
	}{
		{
			disks:   []interface{}{map[string]interface{}{"order": 1, "size_gb": 100, "disk_id": 2001}},
			want:    []clockworks.VMDisk{{SizeGB: 100, DiskId: 2001}},
			wantErr: false,
			msg:     "Single disk",
		},
		{
			disks:   []interface{}{map[string]interface{}{"order": 2, "size_gb": 100}, map[string]interface{}{"order": 1, "size_gb": 150}},
			want:    []clockworks.VMDisk{{SizeGB: 150}, {SizeGB: 100}},
			wantErr: false,
			msg:     "Multiple disks",
		},
	}
	for _, tt := range ts {

		got, err := getVMDisksWithDiskResource(tt.disks)
		if tt.wantErr {
			require.Error(t, err, tt.msg)
		} else {
			require.Equal(t, tt.want, got, tt.msg)
			require.NoError(t, err, tt.msg)
		}
	}
}
