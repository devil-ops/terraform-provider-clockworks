package clockworks

import (
	"errors"
	"sort"

	"gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

/*

NOTE: Disks are confusing. To try and help out keeping everything straight, I'm defining some terms:

Disk Resource - This is the native terraform resource defined as an interface

TFDisk - Struct that represents a disk resource. This is primarily to help make
working with the Disk Resources above more straight forward

VMDisk - Struct that represents a disk in the VM as defined by Clockworks.

*/

// TFDisk - Disk as defined in a terraform .tf file
type TFDisk struct {
	Order  int
	SizeGB int
	Id     int
}

// TODO: vmDisksToDiskResource - Convert a VM disk to a disk resource
func vmDisksToDiskResource(vmds []clockworks.VMDisk) ([]map[string]interface{}, error) {
	var disks []map[string]interface{}
	for di, vmdisk := range vmds {
		d := map[string]interface{}{
			"order":   di + 1,
			"disk_id": vmdisk.DiskId,
			"size_gb": vmdisk.SizeGB,
		}
		disks = append(disks, d)
	}
	return disks, nil
}

func getVMDisksWithTFDisks(tfDisks []TFDisk) ([]clockworks.VMDisk, error) {
	var disks []clockworks.VMDisk
	for _, disk := range tfDisks {
		disks = append(disks, clockworks.VMDisk{
			SizeGB: disk.SizeGB,
			DiskId: disk.Id,
		})
	}
	return disks, nil
}

func getVMDisksWithDiskResource(ds []interface{}) ([]clockworks.VMDisk, error) {
	tfDisks, err := diskResourceToTFDisk(ds)
	if err != nil {
		return nil, err
	}
	disks, err := getVMDisksWithTFDisks(tfDisks)
	if err != nil {
		return nil, err
	}
	return disks, nil
}

// diskResourceToTFDisk - Convert a disk resource to a TFDisk. Disk Resource is
// what we get out of the data interface. TFDisk is just a nice struct to work
// with them with
func diskResourceToTFDisk(dr []interface{}) ([]TFDisk, error) {
	var tfDisks []TFDisk
	for _, disk := range dr {
		var tfDisk TFDisk
		tfDisk.SizeGB = disk.(map[string]interface{})["size_gb"].(int)
		tfDisk.Order = disk.(map[string]interface{})["order"].(int)
		// Do we have an id?
		did, ok := disk.(map[string]interface{})["disk_id"].(int)
		if ok {
			tfDisk.Id = did
		}
		tfDisks = append(tfDisks, tfDisk)
	}
	orderedTFDisks, err := diskOrdering(tfDisks)
	if err != nil {
		return nil, err
	}
	err = validateTFDisks(orderedTFDisks)
	if err != nil {
		return nil, err
	}
	return orderedTFDisks, nil
}

func diskOrdering(disks []TFDisk) ([]TFDisk, error) {
	sort.Slice(disks, func(i, j int) bool {
		return disks[i].Order < disks[j].Order
	})
	return disks, nil
}

func validateTFDisks(disks []TFDisk) error {
	orderNums := make([]int, 0)
	for _, d := range disks {
		if d.Order == 0 {
			return errors.New("disk order cannot be 0")
		}
		orderNums = append(orderNums, d.Order)
	}
	if len(uniqueInts(orderNums)) != len(orderNums) {
		return errors.New("Found repeat ordering in disks")
	}
	if orderNums[0] != 1 {
		return errors.New("First disk must have order 1")
	}
	if orderNums[len(orderNums)-1] != len(orderNums) {
		return errors.New("No skipping order numbers allowed")
	}
	return nil
}

func uniqueInts(intSlice []int) []int {
	keys := make(map[int]bool)
	list := []int{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
