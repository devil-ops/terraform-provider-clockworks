package clockworks

import (
	"os"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

var (
	testAccProviders map[string]*schema.Provider
	testAccProvider  *schema.Provider
)

func init() {
	testAccProvider = Provider()
	testAccProviders = map[string]*schema.Provider{
		"clockworks": testAccProvider,
	}
}

func testAccPreCheck(t *testing.T) {
	if err := os.Getenv("CLOCKWORKS_USERNAME"); err == "" {
		t.Fatal("CLOCKWORKS_USERNAME must be set for acceptance tests")
	}
	if err := os.Getenv("CLOCKWORKS_TOKEN"); err == "" {
		t.Fatal("CLOCKWORKS_TOKEN must be set for acceptance tests")
	}
}
