package clockworks

import (
	"fmt"
	"strings"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	clockworksAPI "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func init() {
	schema.DescriptionKind = schema.StringMarkdown

	schema.SchemaDescriptionBuilder = func(s *schema.Schema) string {
		desc := s.Description
		if s.Default != nil {
			desc += fmt.Sprintf(" Defaults to `%v`.", s.Default)
		}
		if s.Deprecated != "" {
			desc += " " + s.Deprecated
		}
		return strings.TrimSpace(desc)
	}
}

func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("CLOCKWORKS_BASE_URL", "https://clockworks.oit.duke.edu"),
				Description: "URL for Clockworks Server.  Defaults to https://clockworks.oit.duke.edu. Use CLOCKWORKS_BASE_URL environment variable to override.",
			},
			"username": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("CLOCKWORKS_USERNAME", ""),
				Description: "Username for Clockworks calls. Use CLOCKWORKS_USERNAME environment variable.",
			},
			"token": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("CLOCKWORKS_TOKEN", ""),
				Description: "API Token for authenticating against Clockworks. Use CLOCKWORKS_TOKEN environment variable.",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"clockworks_vm": resourceVM(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"clockworks_location": dataSourceLocation(),
			"clockworks_vm":       dataSourceVM(),
			"clockworks_os":       dataSourceOS(),
		},
		ConfigureFunc: providerConfigure,
		// Timeouts:      &schema.ResourceTimeout{Create: schema.DefaultTimeout(45 * time.Minute)},
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	// address := d.Get("address").(string)
	// port := d.Get("port").(int)
	// token := d.Get("token").(string)
	// var diags diag.Diagnostics

	clockworksC := clockworksAPI.NewClient(nil, d.Get("url").(string), d.Get("username").(string), d.Get("token").(string))
	return clockworksC, nil
}
