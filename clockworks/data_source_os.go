package clockworks

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	clockworksAPI "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func dataSourceOS() *schema.Resource {
	return &schema.Resource{
		ReadContext: OSRead,
		Schema: map[string]*schema.Schema{
			"os_id": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "The OS ID name (use this when requesting a new VM)",
			},
			"full_name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The full, human readable description of the OS",
			},
			"clockworks_id": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "Internal numerical clockworks ID of the resource",
			},
			"template": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The VMware template used to deploy this OS",
			},
			"api_only": {
				Type:        schema.TypeBool,
				Computed:    true,
				Description: "If true, OS is not exposed in the web UI",
			},
			"ssi_managed": {
				Type:        schema.TypeBool,
				Computed:    true,
				Description: "If true, OS is is not available for self-admin VMs",
			},
			"sccm_collection_id": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The SCCM collection applied with this OS",
			},
			"movement": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The deployment technique clockworks will use to provision this OS",
			},
			"init_script": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "The recipe clockworks will use to customize the system",
			},
		},
	}
}

func OSRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*clockworksAPI.Client)
	var diags diag.Diagnostics

	osid := d.Get("os_id").(string)
	os, _, err := c.OS.Get(ctx, osid)
	if err != nil {
		return diag.FromErr(err)
	}

	shortcuts := map[string]interface{}{
		"full_name":          os.FullName,
		"clockworks_id":      os.Id,
		"template":           os.Template,
		"api_only":           os.APIOnly,
		"ssi_managed":        os.SSIManaged,
		"sccm_collection_id": os.SCCMCollectionId,
		"movement":           os.Movement,
	}
	for k, v := range shortcuts {
		if err := d.Set(k, v); err != nil {
			return diag.FromErr(err)
		}
	}

	d.SetId(osid)

	return diags
}
