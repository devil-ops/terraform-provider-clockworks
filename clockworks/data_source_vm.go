package clockworks

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	clockworksAPI "gitlab.oit.duke.edu/devil-ops/go-clockworks/clockworks"
)

func dataSourceVM() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceVMRead,
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Name of the VM (Generally the FQDN)",
			},
			"cpu": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "CPU Cores",
			},
			"clockworks_id": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "Id of record inside Clockworks",
			},
			"mem_gb": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "Memory in GB",
			},
			"guest_fqdn": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Guest FQDN",
			},
			"web_url": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Web URL",
			},
			"os_id": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Operating System Id",
			},
			"storage_type": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Storage Type",
			},
			"patch_window_id": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "Patch Window Id",
			},
			"sysadmin_option_id": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "Sysadmin Option Id",
			},
			"disks": {
				Type:        schema.TypeList,
				Computed:    true,
				Description: "List of disks",
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"disk_id": {
							Type:        schema.TypeInt,
							Computed:    true,
							Description: "Disk Id",
						},
						"size_gb": {
							Type:        schema.TypeFloat,
							Computed:    true,
							Description: "Size of the disk in GB",
						},
					},
				},
			},
		},
	}
}

func dataSourceVMRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*clockworksAPI.Client)
	var diags diag.Diagnostics

	name := d.Get("name").(string)
	vm, _, err := c.VM.GetWithFQDN(ctx, name)
	if err != nil {
		return diag.FromErr(err)
	}
	shortcuts := map[string]interface{}{
		"name":               vm.Name,
		"cpu":                vm.CPU,
		"guest_fqdn":         vm.GuestFqdn,
		"mem_gb":             vm.MemGB,
		"patch_window_id":    vm.PatchWindowId,
		"web_url":            vm.WebURL,
		"clockworks_id":      vm.Id,
		"storage_type":       vm.StorageType,
		"sysadmin_option_id": vm.SysadminOptionId,
		"os_id":              vm.OSId,
	}
	for k, v := range shortcuts {
		if err := d.Set(k, v); err != nil {
			return diag.FromErr(err)
		}
	}
	// Figure out these freakin' disks
	disks := make([]map[string]interface{}, 0)
	for _, disk := range vm.Disks {
		disks = append(disks, map[string]interface{}{
			"disk_id": disk.DiskId,
			"size_gb": disk.SizeGB,
		})
	}
	if err := d.Set("disks", disks); err != nil {
		return diag.FromErr(err)
	}

	// Wrap up with an id
	sum := sha256.Sum256([]byte(name))
	d.SetId(fmt.Sprintf("%x", sum))

	return diags
}
